import { Component, OnInit } from '@angular/core';

// Services
import { EquipoService } from "../services/equipo.service";

@Component({
  selector: 'app-nosotros',
  templateUrl: './nosotros.component.html',
  styleUrls: ['./nosotros.component.css']
})
export class NosotrosComponent implements OnInit {

  equipo: any[] = [];

  constructor(private equipoService: EquipoService) {
    this.equipo = equipoService.obtenerEquipo();
   }

  ngOnInit(): void {
  }

}
