import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  title = 'Market';
  show:boolean = true;
  show2:boolean = true;
  activo:string = '';
  primero:string = '';
  segundo:string = '';

  cursos:string[] = ['HTML', 'ANGULAR', 'VUEJS'];
}
