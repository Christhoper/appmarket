import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class EquipoService {

  // Crear objeto a consumir por NosotrosComponent
  equipo: any[] = [{
    name: 'Christhoper',
    especialidad: 'Angular y Vue',
    descripcion: 'Weno para programar'
  },
  {
    name: 'Edu',
    especialidad: 'Angular y Vue',
    descripcion: 'Weno para la tula'
  }];
  constructor() {
    console.log('Funkaaaaaaa');
  }

  obtenerEquipo(){
    return this.equipo;
  }
}
